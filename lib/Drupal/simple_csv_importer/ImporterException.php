<?php

namespace Drupal\simple_csv_importer;

use Exception;

/**
 * Defines our own exception class for exception handling.
 */
class ImporterException extends Exception {

}
