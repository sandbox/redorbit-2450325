<?php

namespace Drupal\simple_csv_importer;

use Exception;

/**
 * Control the import tun.
 */
class Import {

  private $pathname;

  /**
   * Constructor.
   *
   * @param string $pathname
   *   Pathname of the csv file.
   */
  public function __construct($pathname) {

    $this->pathname = $pathname;
  }

  /**
   * Runs the import.
   */
  public function run() {

    drupal_set_time_limit(300);
    ini_set("auto_detect_line_endings", TRUE);

    try {
      $this->initGlobal();
      $csv_class = GlobalData::instance()->config['class_Csv'];
      $csv = new $csv_class($this->pathname);
      Content::setFields($csv->next());
      while ($next = $csv->next()) {
        $content = new Content($next);
        $content->save();
      }
      $csv->close();
    }
    catch (ImporterException $e) {
      drupal_set_message($e->getMessage(), 'error');
      $this->logException($e);
    }
    catch (Exception $e) {
      $msg = t('An unexpected error occurred: @info', array('@info' => $e->getMessage()));
      drupal_set_message($msg, 'error');
      $this->logException($e);
    }
  }

  /**
   * Logs an exception.
   *
   * @param \Exception $e
   *   The exception that has been caught.
   */
  private function logException(Exception $e) {

    $msg = $e->getFile() . '(' . $e->getLine() . '): ' . $e->getMessage();
    watchdog('simple_csv_importer', $msg, array(), WATCHDOG_WARNING);
    if (function_exists('dpm')) {
      // @codingStandardsIgnoreStart
      dpm($msg);
      dpm($e->getTraceAsString());
      // @codingStandardsIgnoreEnd
    }
  }

  /**
   * Initializes the global instance.
   */
  private function initGlobal() {

    GlobalData::instance()->setLanguage($this->pathname);

    $config = new Config();
    $config->load();
    GlobalData::instance()->setConfig($config);
  }

}
