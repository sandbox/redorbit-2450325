<?php

namespace Drupal\simple_csv_importer;

/**
 * Loads and saves the config data to the db.
 */
class Config {

  /**
   * Contains the complete config data.
   *
   * @var array
   *   The config data.
   */
  public $data;

  private $found;

  /**
   * Loads the config data from the db.
   *
   * If no config data found, set default values.
   */
  public function load() {

    // Always start with fresh data. Fields may no more be available due
    // to an update.
    $this->data = array(
      'separator' => ',',
      'delimiter' => '"',
      'id' => '',
      'feedsid' => '',
      'contenttype' => '',
      'mapping' => '',
      'class_Csv' => '',
      'class_Node' => '',
      'class_Field' => '',
    );
    $this->found = FALSE;

    $query = db_select('simple_csv_importer_config', 'c');
    $query
      ->condition('id', 1)
      ->fields('c', array('data'));
    $row = $query->execute()->fetchObject();
    if ($row) {
      $data = json_decode($row->data, TRUE/*assoc*/);
      if (!$data) {
        drupal_set_message(t('Failed to decode json data.'), 'error');
      }
      else {
        $this->found = TRUE;
        foreach (array_keys($this->data) as $key) {
          if (array_key_exists($key, $data)) {
            $this->data[$key] = $data[$key];
          }
        }
      }
    }
  }

  /**
   * Saves the config data to the db.
   */
  public function save() {

    $json_data = json_encode($this->data);

    if ($this->found) {
      db_update('simple_csv_importer_config')
        ->fields(array(
          'data' => $json_data,
          'changed' => time(),
        ))
        ->condition('id', 1)
        ->execute();
    }
    else {
      db_insert('simple_csv_importer_config')
        ->fields(array(
          'id' => 1,
          'data' => $json_data,
          'created' => time(),
          'changed' => time(),
        ))
        ->execute();
    }
  }

}
