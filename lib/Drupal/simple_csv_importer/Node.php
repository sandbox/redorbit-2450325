<?php

namespace Drupal\simple_csv_importer;

use EntityFieldQuery;
use stdClass;

/**
 * Prepares, loads, and saves a node.
 */
class Node {

  /**
   * The drupal node object.
   *
   * @var \stdClass
   */
  public $node;

  public $id;
  private $feedsId;

  /**
   * Constructor.
   */
  public function __construct($id, $feeds_id = '') {

    $this->id = $id;
    $this->feedsId = $feeds_id;
  }

  /**
   * Prepares a node.
   *
   * If the node with the given id information exists, it is loaded.
   * Otherwise an empty node is created.
   */
  public function prepare() {

    $nid = $this->getNidFromId();
    if ($nid) {
      $this->node = node_load($nid);
    }
    if (!$this->node && $this->feedsId) {
      $this->node = $this->getNidFromFeedsId();
    }
    if (!$this->node) {
      // Create an empty node.
      $this->node = new stdClass();
      $this->node->type = GlobalData::instance()->config['contenttype'];
      node_object_prepare($this->node);
      $this->node->nid = 0;
      $this->node->language = GlobalData::instance()->defaultLanguage;
      $this->node->uid = 1;
      // Set published flag.
      $this->node->status = 1;
      $this->node->promote = 0;
      $this->node->comment = 0;
    }
  }

  /**
   * Gets the node id from the id information.
   *
   * If no node can be found, 0 is returned.
   */
  private function getNidFromId() {

    $field_names = GlobalData::instance()->mapping(GlobalData::instance()->config['id']);
    $field_name = $field_names[0];

    // The id field must not be translatable: one id for all languages.
    $field_info = field_info_field($field_name);
    if ($field_info['translatable']) {
      throw new ImporterException(t('The id field "@field" must not be translatable.', array('@field' => $field_name)));
    }

    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', GlobalData::instance()->config['contenttype']);
    if ($field_name == 'title') {
      $query->propertyCondition('title', $this->id);
    }
    else {
      $query->fieldCondition($field_name, 'value', $this->id);
    }

    $result = $query->execute();
    if (isset($result['node'])) {
      if (count($result['node']) > 1) {
        // Only one element is expected.
        throw new ImporterException(t('Internal error: more than one nodes with the id "@id".', array('@id' => $this->id)));
      }
      $node = array_shift($result['node']);
      return $node->nid;
    }
    else {
      return 0;
    }
  }

  /**
   * Tries to get a node id from a feeds id.
   *
   * If no node can be found, 0 is returned.
   */
  private function getNidFromFeedsId() {

    $query = db_select('feeds_item');
    $query
      ->condition('guid', $this->feedsId)
      ->fields('feeds_item', array('entity_id'));
    $row = $query->execute()->fetchObject();
    if ($row) {
      return $row->entity_id;
    }
    else {
      return 0;
    }
  }

  /**
   * Saves a node.
   */
  public function save() {

    $default = GlobalData::instance()->defaultLanguage;

    // Set a default title if no title found. Set $title for later use.
    $title = 'Unknown title';
    if (property_exists($this->node, 'title_field')) {
      if (array_key_exists($default, $this->node->title_field)) {
        $title = $this->node->title_field[$default][0]['value'];
      }
      else {
        $this->node->title_field[$default][0]['value'] = $title;
      }
    }
    else {
      if (property_exists($this->node, 'title') && $this->node->title != '') {
        $title = $this->node->title;
      }
      else {
        $this->node->title = $title;
      }
    }

    $update = (bool) $this->node->nid;

    // dpm($this->node);
    // Dump.
    node_save($this->node);
    if (!$this->node->nid) {
      throw new ImporterException(t('Failed to save node "@title".', array('@title' => $title)));
    }

    $node_info = $title . ' (' . $this->node->nid . ')';
    if ($update) {
      drupal_set_message(t('Node "@info" updated.', array('@info' => $node_info)));
    }
    else {
      drupal_set_message(t('Node "@info" created.', array('@info' => $node_info)));
    }
  }

}
