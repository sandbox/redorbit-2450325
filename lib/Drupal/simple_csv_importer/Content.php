<?php

namespace Drupal\simple_csv_importer;

/**
 * Gets the contents of a csv line and cares for them.
 */
class Content {

  private static $fields;
  private static $count;

  private $elems;

  /**
   * Stores the values of a csv line.
   *
   * @param array $elems
   *   The values of the csv line.
   */
  public function __construct(array $elems) {

    $this->elems = $elems;
  }

  /**
   * Saves the contents contained in a csv line.
   */
  public function save() {

    // dpm($this->elems);
    // Simplify the code.
    if (GlobalData::instance()->isTranslation) {
      $language = GlobalData::instance()->language;
      $default_language = GlobalData::instance()->defaultLanguage;
    }
    else {
      $language = LANGUAGE_NONE;
      $default_language = LANGUAGE_NONE;
    }

    $id = $this->getElem(GlobalData::instance()->config['id']);
    if ($id == '') {
      throw new ImporterException(t('Id column "@id": The value must not be empty.', array('@id' => GlobalData::instance()->config['id'])));
    }
    $feeds_id = GlobalData::instance()->config['feedsid'];

    $node_class = GlobalData::instance()->config['class_Node'];
    $node = new $node_class($id, $feeds_id);
    $node->prepare($id);
    // dpm($node);
    // Dump.
    if (($language != $default_language) && !$node->node->nid) {
      throw new ImporterException(t('Content cannot be imported for language "@lang" (Id="@id"). There must be a content of the standard language "@defaultLang" first.',
          array(
            '@lang' => $language,
            '@id' => $node->id,
            '@defaultLang' => $default_language,
          )));
    }

    foreach (GlobalData::instance()->mapping as $key => $field_names) {
      foreach ($field_names as $field_name) {
        if (($language == $default_language)
          && (($field_name == 'title') || ($field_name == 'title_field'))) {
          $node->node->title = $this->getElem($key);
          continue;
        }
        $field_class = GlobalData::instance()->config['class_Field'];
        $field = new $field_class($field_name, $node);
        $value = $field->getValue($this->getElem($key));
        if ($value) {
          if ($field->isTranslatable) {
            $node->node->{$field_name}[$language][0] = $value;
          }
          else {
            if ($language == $default_language) {
              $node->node->{$field_name}[LANGUAGE_NONE][0] = $value;
            }
          }
        }
        else {
          // What to do if no value is set?
          // 1) Ignore the original value?
          // 2) Delete the original value?
          // We decided to choose 2)
          if ($field->isTranslatable) {
            if ($this->isEmptyField($node->node->{$field_name})) {
              $node->node->{$field_name} = array();
            }
          }
          else {
            if ($language == $default_language) {
              $node->node->{$field_name} = array();
            }
          }
        }
      }
    }

    // dpm($node);
    // Dump.
    if ($language != $default_language) {
      if ($node->node->language != $default_language) {
        throw new ImporterException(t('The node\'s language is "@lang", but must be the default language "@def_lang".',
          array('@lang' => $node->node->language, '@def_lang' => $default_language)));
      }
      $handler = entity_translation_get_handler('node', $node->node);
      $translation = array(
        'translate' => 0,
        'status' => 1,
        'language' => $language,
        'source' => $node->node->language,
      );
      $handler->setTranslation($translation, $node->node);
    }

    $node->save();
  }

  /**
   * Stores the column names of the csv file.
   *
   * Initializes the "self::$fields" array that contains a mapping of the
   * columns of the first line of the csv file to an index.
   * E.g. "Produkt-ID" => 3
   *
   * @param array $fields
   *   The column names.
   */
  public static function setFields(array $fields) {

    // dpm($fields);
    // Dump.
    self::$count = count($fields);
    self::$fields = array();
    $i = 0;
    foreach ($fields as $csv_field) {
      $trimmed_field = trim($csv_field);
      self::$fields[$trimmed_field] = $i++;
    }
  }

  /**
   * Gets the value for a csv key.
   *
   * @param string $name
   *   The column name in the csv file.
   *
   * @returns
   *   The string value of the column.
   */
  private function getElem($name) {

    if (array_key_exists($name, self::$fields)) {
      return trim($this->elems[self::$fields[$name]]);
    }

    throw new ImporterException(t('Column "@name" not found.', array('@name' => $name)));
  }

  /**
   * Check if for all languages there are only empty strings.
   *
   * @param array $field
   *   The field of the node.
   *
   * @return bool
   *   Return TRUE if all values are empty strings.
   */
  private function isEmptyField(array $field) {

    foreach ($field as $value) {
      if (array_key_exists('value', $value[0]) && ($value[0]['value'] === '')) {
        // empty.
      }
      else {
        return FALSE;
      }
    }

    return TRUE;
  }

}
