<?php

namespace Drupal\simple_csv_importer;

/**
 * Singleton that contains glonbal data.
 */
class GlobalData {

  /**
   * The configuration data stored in the db.
   *
   * @var array
   */
  public $config;

  /**
   * The language of the csv that is imported.
   *
   * @var string
   */
  public $language;

  /**
   * The default language set by the user or language undefined.
   *
   * @var string
   */
  public $defaultLanguage;

  /**
   * The mapping as an associative array.
   *
   * @var array
   */
  public $mapping;

  /**
   * Whether the content is translatable.
   *
   * @var bool
   */
  public $isTranslation;

  /**
   * Define our own constant.
   *
   * We do not want to be dependent on "entity_translation".
   */
  const SIMPLE_CSV_IMPORTER_ENTITY_TRANSLATION_ENABLED = 4;

  /**
   * An instance of the singleton.
   *
   * @var \Drupal\simple_csv_importer\GlobalData
   */
  private static $instance;

  /**
   * Constructor.
   */
  private function __construct() {

    if (!module_exists('locale') && !module_exists('entity_translation')) {
      $this->defaultLanguage = LANGUAGE_NONE;
    }
    elseif (module_exists('locale') && module_exists('entity_translation')) {
      $default_language = variable_get('language_default');
      if (!$default_language) {
        throw new ImporterException(t('Please define a default language first.'));
      }
      $this->defaultLanguage = $default_language->language;
    }
    else {
      throw new ImporterException(t('To use this module the modules "locale" and "entity_translation" must be enabled or both disabled.'));
    }
  }

  /**
   * Given the csv filename, sets the language.
   */
  public function setLanguage($pathname) {

    if ($this->defaultLanguage == LANGUAGE_NONE) {
      $this->language = LANGUAGE_NONE;
    }
    else {
      $language = substr($pathname, -6, 2);
      $this->language = strtolower($language);
    }
  }

  /**
   * Sets the config property.
   */
  public function setConfig($config) {

    $this->config = $config->data;

    $this->mapping = array();
    $mapping_lines = explode("\n", $config->data['mapping']);
    foreach ($mapping_lines as $line) {
      $trimmed_line = trim($line);
      if ($trimmed_line) {
        $parts = explode('=>', $line);
        // A key may be mapped to multiple fields, therefore we store the
        // field names in an array.
        $this->mapping[trim($parts[0])][] = trim($parts[1]);
      }
    }

    // Initialize all class names.
    foreach (array_keys($this->config) as $key) {
      if (substr($key, 0, 6) == 'class_') {
        $classname = $this->config[$key];
        if (!$classname) {
          $this->config[$key] = '\\Drupal\\simple_csv_importer\\' . substr($key, 6);
        }
      }
    }

    $this->setTranslation();
  }

  /**
   * Sets the isTranslation property.
   */
  private function setTranslation() {

    $content_type = $this->config['contenttype'];
    $translation = variable_get('language_content_type_' . $content_type, 0);
    if ($translation == self::SIMPLE_CSV_IMPORTER_ENTITY_TRANSLATION_ENABLED) {
      $this->isTranslation = TRUE;
    }
    elseif ($translation == 0) {
      $this->isTranslation = FALSE;
    }
    else {
      throw new ImporterException(t('Cannot handle the selected translation mode for the content type.'));
    }
  }

  /**
   * Returns an instance of global.
   */
  public static function instance() {

    if (!self::$instance) {
      self::$instance = new GlobalData();
    }

    return self::$instance;
  }

  /**
   * Access method for a key in the mapping.
   *
   * @param string $key
   *   The key (csv column).
   *
   * @return string
   *   The field name or an exception is thrown.
   */
  public function mapping($key) {

    if (array_key_exists($key, $this->mapping)) {
      return $this->mapping[$key];
    }
    else {
      throw new ImporterException(t('The csv column "@key" is not defined in the mapping.', array('@key' => $key)));
    }
  }

}
