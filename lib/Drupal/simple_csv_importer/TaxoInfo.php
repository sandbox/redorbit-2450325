<?php

namespace Drupal\simple_csv_importer;

use stdClass;

/**
 * Checks and updates term for a vocabulary.
 *
 * We don't see the necessity of caching for an import.
 * Drupal's caching should be sufficient.
 */
class TaxoInfo {

  private $vocabulary;
  private $language;
  private $defaultLanguage;

  /**
   * Constructor.
   *
   * @param string $name
   *   The vocabulary's machine name.
   */
  public function __construct($name) {

    $this->vocabulary = taxonomy_vocabulary_machine_name_load($name);
    if (!$this->vocabulary) {
      throw new ImporterException(t('Vocabulary with machine name "@name" not found.', array('@name' => $name)));
    }

    $entity_translation = variable_get('entity_translation_settings_taxonomy_term__' . $name);
    if ($entity_translation) {
      $this->language = GlobalData::instance()->language;
      $this->defaultLanguage = GlobalData::instance()->defaultLanguage;
    }
    else {
      $this->language = LANGUAGE_NONE;
      $this->defaultLanguage = LANGUAGE_NONE;
    }
  }

  /**
   * Returns the tid for a term or 0.
   *
   * If an old tid is given, check whether the given name is allowed. In
   * case of a translation the name is written to the term for the language.
   *
   * @param string $name
   *   The term's name.
   * @param int $old_tid
   *   The tid found in the node (may be 0).
   * @param Node $node
   *   The node for better error diagnostics.
   */
  public function getTid($name, $old_tid, Node $node) {

    if (($this->defaultLanguage == LANGUAGE_NONE)
      && (GlobalData::instance()->language != GlobalData::instance()->defaultLanguage)) {
      // This means: we have a taxonomy without translations. In this case
      // we only define/modify terms in case the language is the default
      // language. Return 0 to indicate that we ignore the value.
      return 0;
    }

    // dpm(func_get_args());
    // If there is no "oldTid" given, we search for an already existing term
    // of name "$name" or we create a new one.
    if (!$old_tid) {
      if ($this->language == $this->defaultLanguage) {
        $term = $this->findOrCreateTerm($name);
        return $term->tid;
      }
      else {
        throw new ImporterException(t('Can only create a new taxonomy term for the default language "@lang" (Id="@id", nid=@nid).',
          array(
            '@lang' => $this->defaultLanguage,
            '@id' => $node->id,
            '@nid' => $node->node->nid,
          )));
      }
    }

    // An old tid was found, look for the term.
    $old_term = $this->readTerm($old_tid);
    if (!$old_term) {
      // Old term not found. Term deleted? Create a new one.
      $tid = $this->getTid($name, 0, $node);
      return $tid;
    }

    // An old term was found.
    if ($this->language == $this->defaultLanguage) {
      // Check if the old term's name is the expected new one.
      // Maybe the taxonomy term has changed or there is a typo.
      if ($this->language == LANGUAGE_NONE) {
        $old_name = $old_term->name;
      }
      else {
        // There may occur a situation during the switch to entity translation
        // that there are still terms that have language "none". Bad luck!
        if (!array_key_exists($this->defaultLanguage, $old_term->name_field)) {
          drupal_set_message(t('The taxonomy term "@name" (tid=@tid) is not defined for language "@lang" but for "@found_lang" (Id="@id", nid=@nid). Please delete and recreate term or fix in the database.',
            array(
              '@name' => $name,
              '@tid' => $old_term->tid,
              '@lang' => $this->language,
              '@found_lang' => $old_term->language,
              '@id' => $node->id,
              '@nid' => $node->node->nid,
            )), 'error');
          $old_name = $name;
        }
        else {
          $old_name = $old_term->name_field[$this->defaultLanguage][0]['value'];
        }
      }
      if ($old_name == $name) {
        return $old_tid;
      }
      else {
        $term = $this->changeTerm($old_name, $name);
        $tid = $term->tid;
        return $tid;
      }
    }

    // This is the non default language: update the term with the given name
    // for this language.
    $update = FALSE;
    if (array_key_exists($this->language, $old_term->name_field)) {
      $old_name = $old_term->name_field[$this->language][0]['value'];
      if ($old_name != $name) {
        // Update the name.
        $update = TRUE;
      }
    }
    else {
      // Create a translation.
      $update = TRUE;
    }
    if ($update) {
      $this->updateTerm($old_term, $name, $this->language);
    }
    return $old_tid;
  }

  /**
   * Reads the term from the vocabulary.
   *
   * @param int $tid
   *   The term's id.
   */
  private function readTerm($tid) {

    $term = taxonomy_term_load($tid);
    if (!$term) {
      drupal_set_message(t('Vocabulary "@voc": The term with id @tid is no more available.',
        array('@voc' => $this->vocabulary->name, '@tid' => $tid)), 'warning');
    }

    return $term;
  }

  /**
   * Searches for the term and creates a new term if not found.
   *
   * @param string $name
   *   The term's name.
   */
  private function findOrCreateTerm($name) {

    $search = $this->getTermByName($name);
    if ($search) {
      return $search;
    }

    // Create a new term with the given name.
    $term = new stdClass();
    $term->vid = $this->vocabulary->vid;
    $term->name = $name;
    $term->description = '';
    $term->format = 'full_html';
    $term->language = $this->defaultLanguage;
    $term->weight = 1;

    // "name_field" would be set automatically, but not the "format" field.
    $term->name_field[$this->defaultLanguage][0] = array(
      'value' => $name,
      'format' => 'full_html',
    );

    taxonomy_term_save($term);

    if ($term->tid) {
      drupal_set_message(t('Vocabulary "@voc": New term "@term" created.',
        array('@voc' => $this->vocabulary->name, '@term' => $name)));
    }
    else {
      throw new ImporterException(t('Could not create term "@term" for vocabulary "@vocabulary".',
        array('@term' => $name, '@vocabulary' => $this->vocabulary->name)));
    }

    return $term;
  }

  /**
   * Changes a term if the found name and new one don't match.
   *
   * @param string $old_name
   *   The name found by the node.
   * @param string $new_name
   *   The name given in the csv file.
   */
  private function changeTerm($old_name, $new_name) {

    $search = $this->getTermByName($new_name);
    if ($search) {
      return $search;
    }
    elseif ($this->language == LANGUAGE_NONE) {
      // We create a new term.
      return $this->findOrCreateTerm($new_name);
    }
    else {
      // In case of entity translation we reject the automatic creation,
      // because the translations would not be modified. This may cause
      // strange effects.
      throw new ImporterException(t('Cannot proceed: Vocabulary "@vocabulary": The term "@old_name" would be replaced by "@new_name", but such a term does not exist. If you want to rename the term or really create a new one, please use the admin ui.',
        array(
          '@vocabulary' => $this->vocabulary->name,
          '@old_name' => $old_name,
          '@new_name' => $new_name,
        )));
    }
  }

  /**
   * Returns term by its name.
   *
   * @param string $name
   *   The term's name.
   */
  private function getTermByName($name) {

    $terms = taxonomy_get_term_by_name($name, $this->vocabulary->machine_name);
    if (count($terms) > 1) {
      throw new ImporterException(t('Cannot proceed: Vocabulary "@vocabulary": There are multiple terms with the name "@name". Please fix the problem using the admin ui.',
        array('@vocabulary' => $this->vocabulary->name, '@name' => $name)));
    }

    if (!empty($terms)) {
      return array_shift($terms);
    }
    else {
      return NULL;
    }
  }

  /**
   * Add a new translation or update an existing one.
   *
   * @param object $term
   *   The taxonomy term.
   * @param string $name
   *   The translated name (value).
   * @param string $language
   *   The language for the translation.
   *
   * @see: https://www.drupal.org/node/1069774
   *   How to add a translation to an entity programmatically
   */
  private function updateTerm($term, $name, $language) {

    $translation = array(
      'translate' => 0,
      'status' => 1,
      // Here is the language you're translating to.
      'language' => $language,
      // Here is the source language.
      'source' => $this->defaultLanguage,
    );

    // Get the translation handler in place.
    $handler = entity_translation_get_handler('taxonomy_term', $term);

    // This is the array structure for the field you are translating, in our
    // case the field name was "name_field", $value is the translated string.
    $values = array(
      'name_field' => array(
        $language => array(
          '0' => array(
            'value' => $name,
            'format' => 'full_html',
          ),
        ),
      ),
    );

    // Finally you set the translation and save the object.
    $handler->setTranslation($translation, $values);

    taxonomy_term_save($term);
  }

}
