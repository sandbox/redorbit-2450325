<?php

namespace Drupal\simple_csv_importer;

/**
 * Creates a structure suited to be stored as a Drupal field value.
 */
class Field {

  private $fieldName;
  private $node;

  public $isTranslatable;

  /**
   * Constructor.
   *
   * @param string $field_name
   *   The name of the field.
   * @param object $node
   *   The node (a new one or an existing one).
   */
  public function __construct($field_name, $node) {

    $this->fieldName = $field_name;
    $this->node = $node;
  }

  /**
   * Given a value an array suitable for a Drupal field is return.
   *
   * E.g. field is an integer field, the value is 7, then
   * <pre>
   * array(
   *   'value' => 7,
   * )
   * </pre>
   * is returned.
   *
   * @param string $value
   *   The value from the csv file.
   */
  public function getValue($value) {

    $this->fixValue($this->fieldName, $value);

    // dpm("fieldName: '{$this->fieldName}', value: '$value'");
    // dpm($this->node->node);
    // Dump.
    if (($this->fieldName != 'title_field')
      && ($this->fieldName != 'body')
      && (strncmp($this->fieldName, 'field_', 6) != 0)) {
      return NULL;
    }

    if ($value === '') {
      return NULL;
    }

    // Let's check what kind of field this is.
    $field_info = field_info_field($this->fieldName);
    $this->isTranslatable = $field_info['translatable'];
    // dpm("Field name: {$this->fieldName}, Value: $value"); dpm($field_info);
    // Dump.
    $result = array();
    switch ($field_info['type']) {
      case 'number_integer':
        $result['value'] = (int) $value;
        break;

      case 'taxonomy_term_reference':
        $this->checkLangUndef();
        $vocabulary = $field_info['settings']['allowed_values'][0]['vocabulary'];
        $result = $this->taxonomyReference($value, $vocabulary, 'tid');
        break;

      case 'entityreference':
        if ($field_info['settings']['target_type'] == 'taxonomy_term') {
          $keys = array_keys($field_info['settings']['handler_settings']['target_bundles']);
          $vocabulary = $keys[0];
          $result = $this->taxonomyReference($value, $vocabulary, 'target_id');
        }
        else {
          throw new ImporterException(t('Cannot handle field type "@type" for field "@field" where target is no taxonomy.',
            array('@type' => $field_info['type'], '@field' => $this->fieldName)));
        }
        break;

      case 'text':
      case 'text_long':
      case 'text_with_summary':
        $result = $this->textResult($field_info, $value);
        break;

      case 'datestamp':
        $result['value'] = $value;
        break;

      case 'image':
      case 'file':
        $result = $this->fileResult($field_info, $value);
        break;

      default:
        throw new ImporterException(t('Cannot handle field type "@type" for field "@field".',
          array('@type' => $field_info['type'], '@field' => $this->fieldName)));
    }

    // dpm("Result:\n" . print_r($result, TRUE));
    // Dump.
    return $result;
  }

  /**
   * Returns the structure for a taxonomy reference or NULL.
   *
   * @param string $value
   *   The value (name) of the taxonomy term.
   * @param string $vocabulary
   *   The name of taxonomy vocabulary.
   * @param string $key
   *   The key to be used in the result, e.g. "tid".
   */
  private function taxonomyReference($value, $vocabulary, $key) {

    if (property_exists($this->node->node, $this->fieldName)
      && array_key_exists(LANGUAGE_NONE, $this->node->node->{$this->fieldName})) {
      $tid = $this->node->node->{$this->fieldName}[LANGUAGE_NONE][0][$key];
    }
    else {
      $tid = 0;
    }
    $taxo_info = new TaxoInfo($vocabulary);
    $tid = $taxo_info->getTid($value, $tid, $this->node);
    if ($tid) {
      return array(
        $key => $tid,
      );
    }
    else {
      return NULL;
    }
  }

  /**
   * Returns the values for text.
   *
   * Makes a guess (maybe right or wrong) whether this is plain text or html.
   *
   * 2015-04-29: Fix: Also consider fields of "text" type, they, too, can
   *   have html format.
   * 2015-04-29: Fix: User uses the glossary that inserts links into the text
   *   fields. Therefore all formats will be set to "full_html".
   * TODO: Find out how to9 make sure that only "plain_text" is allowed.
   */
  protected function textResult(&$field_info, $value) {

    return array(
      'value' => $value,
      'format' => 'full_html',
    );

    /*
    $result['value'] = $value;
    $stripped = strip_tags($value);
    if ($stripped != $value) {
    $result['format'] = 'full_html';
    }
    else {
    $result['format'] = 'plain_text';
    }

    return $result;
     */
  }

  /**
   * Returns the values for a file.
   *
   * @param array $field_info
   *   The field info.
   * @param string $filename
   *   The filename of the file (read from the csv).
   */
  protected function fileResult(array &$field_info, $filename) {

    if (!$filename) {
      return NULL;
    }

    if (!file_exists($filename)) {
      throw new ImporterException(t('File "@name" not found.', array('@name' => $filename)));
    }

    $uri_scheme = $field_info['settings']['uri_scheme'];

    $instances_info = field_read_instances(array(
      'field_name' => $this->fieldName,
      'bundle' => GlobalData::instance()->config['contenttype'],
    ));
    if (empty($instances_info)) {
      throw new ImporterException(t('Internal error: did not find instance info for "@field".', array('@field' => $this->fieldName)));
    }
    $instance_info = $instances_info[0];
    $directory = $instance_info['settings']['file_directory'];
    $extensions = $instance_info['settings']['file_extensions'];

    $this->checkFileExtension($filename, $extensions);
    $target = $this->fileTarget($uri_scheme, $directory, $filename);

    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $file_info = finfo_file($finfo, $filename);
    finfo_close($finfo);
    $file = (object) array(
      'uid' => 1,
      'uri' => $filename,
      'filemime' => $file_info,
      'status' => 1,
    );

    $target_file = file_copy($file, $target, FILE_EXISTS_RENAME);
    if (!$target_file) {
      throw new ImporterException(t('Could not file_copy "@source" to "@target".',
        array('@source' => $filename, '@target' => $target)));
    }

    if ($field_info['type'] == 'file') {
      $target_file->display = 0;
    }
    elseif ($field_info['type'] == 'image') {
      $target_file->alt = basename($filename);
    }

    return (array) $target_file;
  }

  /**
   * Checks if the file extension is valid.
   *
   * @param string $filename
   *   The filename to be checked.
   * @param string $extension_string
   *   String with allowed extensions.
   */
  protected function checkFileExtension($filename, $extension_string) {

    $trimmed_extensions_string = trim($extension_string);
    if (!$trimmed_extensions_string) {
      return;
    }
    $extensions = explode(' ', $trimmed_extensions_string);

    $found = FALSE;
    $i = strrpos($filename, '.');
    if ($i) {
      $found = in_array(substr($filename, $i + 1), $extensions);
    }
    if (!$found) {
      throw new ImporterException(t('Only files with the following extensions are allowed: %files-allowed.', array('%files-allowed' => $extension_string)));
    }
  }

  /**
   * Returns the file target's filename.
   *
   * @param string $uri_scheme
   *   The uri scheme, e.g. "private".
   * @param string $directory
   *   The directory name.
   * @param string $source
   *   The filename as read from the csv.
   */
  protected function fileTarget($uri_scheme, $directory, $source) {

    $basename = basename($source);

    // We do not support tokens for the directory.
    $dir_uri = $uri_scheme . '://' . $directory;
    if (!file_prepare_directory($dir_uri, FILE_CREATE_DIRECTORY)) {
      throw new ImporterException(t('Failed to create directory "@dir".', array('@dir' => $dir_uri)));
    }

    return $dir_uri . '/' . $basename;
  }

  /**
   * Modifies a value depending on the field name.
   *
   * This method may be overridden if needed, e.g. convert a date string
   * into a timestamp.
   *
   * @param string $field_name
   *   The field name.
   * @param string $value
   *   The value read from the csv.
   */
  protected function fixValue($field_name, &$value) {

  }

  /**
   * Checks if the language is undefined.
   *
   * Throws an exception if this is not true.
   */
  protected function checkLangUndef() {

    if ($this->isTranslatable) {
      throw new ImporterException(t('The field "@field" must not be translatable (feature not implemented).', array('@field' => $this->fieldName)));
    }
  }

}
