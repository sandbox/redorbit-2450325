<?php

namespace Drupal\simple_csv_importer;

/**
 * Class to access the csv file.
 */
class Csv {

  private $pathname;
  private $file;
  private $lineNo;
  private $fields;

  /**
   * Opens the csv file and prepares it for reading.
   *
   * @param string $pathname
   *   The csv file's pathname.
   */
  public function __construct($pathname) {

    if (!$pathname) {
      throw new ImporterException(t('Csv pathname missing.'));
    }
    $this->pathname = realpath($pathname);
    if (!$this->pathname) {
      throw new ImporterException(t('File "@pathname" not found.', array('@pathname' => $pathname)));
    }
    $this->file = fopen($this->pathname, 'r');
    if (!$this->file) {
      throw new ImporterException(t('File "@pathname" could not be opened.', array('@pathname' => $this->pathname)));
    }
    $this->lineNo = 0;
  }

  /**
   * Returns the values of the next csv line or FALSE if EOF.
   */
  public function next() {

    $result = $this->nextLine();
    if (!$result) {
      return FALSE;
    }

    if ($this->lineNo == 1) {
      $this->fields = count($result);
    }
    else {
      $fields = count($result);
      if ($fields != $this->fields) {
        throw new ImporterException(t('Number of elements of first line (@fields) and current element differ:\n@result',
          array('@fields' => $this->fields, '@result' => print_r($result, TRUE))));
      }
    }

    return $result;
  }

  /**
   * Returns the fields of the next line of the csv file.
   */
  protected function nextLine() {

    $fields = fgetcsv($this->file, 0, GlobalData::instance()->config['separator'],
      GlobalData::instance()->config['delimiter']);
    if ($fields) {
      $this->lineNo++;

      if (!$this->isUtf8(implode('//', $fields))) {
        drupal_set_message(t('Line @number contains invalid UTF8 characters.', array('@number' => $this->lineNo)), 'warning');
      }
    }

    return $fields;
  }

  /**
   * Closes the csv file.
   */
  public function close() {

    fclose($this->file);
  }

  /**
   * Checks if the string is valid UTF8.
   *
   * @param string $string
   *   The string to check.
   *
   * @return bool
   *   Whether UTF8.
   */
  protected function isUtf8($string) {

    return (bool) preg_match('//u', serialize($string));
  }

}
