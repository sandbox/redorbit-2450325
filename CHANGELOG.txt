2015-08-15
- Permissions split for configuring the importer and for running the import.
- Assertion removed from test because of a bug in Drupal or other module.

2015-07-28
- Better error diagostics.
  If the original taxonomy item is not defined for the default language the
  error message now is e.g.:
  The taxonomy term "123 series" (tid=314) is not defined for language "en" but
  for "de" (Id="intelli20", nid=1569). Please delete and recreate term or fix
  in the database.
- Better error diagnostics.
  If the default content is missing, the id now is displayed, too, e.g.:
  Content cannot be imported for language "de" (Id="intelli20"). There must be
  a content of the standard language "en" first.
