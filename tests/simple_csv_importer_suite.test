<?php

/**
 * @file
 * Contains simple_csv_importer_suite.test.
 */

/**
 * Test some scenarios with text fields.
 */
class SimpleCsvImporterTextTestCase extends SimpleCsvImporterTestCase {

  /**
   * Method getInfo.
   */
  public static function getInfo() {

    return array(
      'name' => 'Import csv with ET for text',
      'description' => 'Import a csv with a text field using entity translation.',
      'group' => 'Simple CSV Importer',
    );
  }

  /**
   * Configure a content type with two text fields.
   */
  protected function configureContentType() {

    parent::configureContentType();
    $this->addField('Test Info', 'text', TRUE/*translatable*/);
    $this->addField('Test UND', 'text', FALSE/*translatable*/);
  }

  /**
   * Tests the case with one English csv file.
   */
  public function testTextEnglish() {

    $this->configText();

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/text-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // DEBUG Show node page.
    $this->drupalGet("node/1/edit");

    $nid = $this->getNidById('field_test_id', 'Text1');
    $this->drupalGet("node/$nid/edit");
    $this->assertRaw('@EN@This is the simple info.');
  }

  /**
   * Tests the case with an English and a German csv.
   */
  public function testTextEnglishGerman() {

    $this->configText();

    $edit_en = array();
    $edit_en['files[file]'] = dirname(__FILE__) . '/data/text-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit_en, t('Save configuration'));
    $this->assertNoMessages();

    $edit_de = array();
    $edit_de['files[file]'] = dirname(__FILE__) . '/data/text-de.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit_de, t('Save configuration'));
    $this->assertNoMessages();

    // Show the node as English/German page.
    $nid = $this->getNidById('field_test_id', 'Text1');
    $this->drupalGet("node/$nid/edit");
    $this->assertRaw('@EN@This is the simple info.');
    $this->assertRaw('@UND@This is language neutral.');
    $this->drupalGet("de/node/$nid/edit");
    $this->assertRaw('@DE@Dies ist die einfache Info.');
    $this->assertRaw('@UND@This is language neutral.');
  }

  /**
   * Configures the importer for the text test.
   */
  private function configText() {

    $edit = array();
    $edit['separator'] = ';';
    $edit['id'] = 'Id';
    $edit['contenttype'] = 'page';
    $edit['mapping'] = <<<EOF
Info => field_test_info
Und => field_test_und
Id => field_test_id
EOF;
    $this->drupalPost('admin/config/system/simple-csv-importer/config', $edit, t('Save configuration'));
    $this->assertRaw(t('The configuration options have been saved.'));
    $this->assertNoMessages();
  }

}

/**
 * Test some scenarios with taxonomy reference fields.
 */
class SimpleCsvImporterTaxonomyTestCase extends SimpleCsvImporterTestCase {

  /**
   * Get info.
   */
  public static function getInfo() {

    return array(
      'name' => 'Import csv with ET for taxonomy terms',
      'description' => 'Import a csv with a taxonomy reference field using entity translation.',
      'group' => 'Simple CSV Importer',
    );
  }

  /**
   * Configures a content type with a taxonomy term reference.
   */
  protected function configureContentType() {

    parent::configureContentType();
    $this->configureTaxonomy();
    $this->addField('Test Tag', 'taxonomy_term_reference', FALSE/*translatable*/);
  }

  /**
   * Configures the taxonomy for entity translation.
   */
  private function configureTaxonomy() {

    $edit1 = array();
    $edit1['entity_translation_entity_types[taxonomy_term]'] = 1;
    $this->drupalPost('admin/config/regional/entity_translation', $edit1, t('Save configuration'));

    $edit2 = array();
    $edit2['entity_translation_settings_taxonomy_term__tags[hide_language_selector]'] = 0;
    $edit2['entity_translation_settings_taxonomy_term__tags[exclude_language_none]'] = 1;
    $edit2['entity_translation_settings_taxonomy_term__tags[lock_language]'] = 1;
    $this->drupalPost('admin/config/regional/entity_translation', $edit2, t('Save configuration'));

    foreach (array('name', 'description') as $field) {
      $url = 'admin/structure/taxonomy/tags/fields/replace/' . $field;
      $this->drupalGet($url);
      $this->drupalPost($url, array('enabled' => 1), t('Save settings'));
    }
    $this->drupalGet('admin/structure/taxonomy/tags/fields');
  }

  /**
   * Tests the case with one English csv file.
   */
  public function testTaxonomyEnglish() {

    $this->configTaxonomy();

    $this->twoNewEnglishTerms();
    $this->modifyEnglishTermWithExisting();
    $this->modifyEnglishTermWithNew();
  }

  /**
   * Imports a csv with two news English terms.
   */
  private function twoNewEnglishTerms() {

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/taxonomy-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // Show node pages.
    $nid1 = $this->getNidById('field_test_id', 'Taxonomy1');
    $this->drupalGet("node/$nid1/edit");
    $this->assertRaw('@EN@First');
    $nid2 = $this->getNidById('field_test_id', 'Taxonomy2');
    $this->drupalGet("node/$nid2/edit");
    $this->assertRaw('@EN@Second');
  }

  /**
   * Imports a csv file to modify an English term with an existing one.
   */
  private function modifyEnglishTermWithExisting() {

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/taxonomy-me-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // Show node pages.
    $nid1 = $this->getNidById('field_test_id', 'Taxonomy1');
    $this->drupalGet("node/$nid1/edit");
    $this->assertRaw('@EN@Second');
    $nid2 = $this->getNidById('field_test_id', 'Taxonomy2');
    $this->drupalGet("node/$nid2/edit");
    $this->assertRaw('@EN@Second');
  }

  /**
   * Imports a csv file to replace an existing term by a new one (not allowed).
   */
  private function modifyEnglishTermWithNew() {

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/taxonomy-mn-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertRaw('If you want to rename the term or really create a new one, please use the admin ui.');

    // Show node pages.
    $nid = $this->getNidById('field_test_id', 'Taxonomy1');
    $this->drupalGet("node/$nid/edit");
    $this->assertRaw('@EN@Second');
  }

  /**
   * Tests the case with an English and a German csv file.
   */
  public function testTaxonomyEnglishGerman() {

    $this->configTaxonomy();

    $this->twoNewEnglishTerms();

    $this->twoGermanTerms();
    $this->updateGermanTerm();
  }

  /**
   * Imports a csv with two German terms.
   */
  private function twoGermanTerms() {

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/taxonomy-de.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // Show node pages.
    $nid1 = $this->getNidById('field_test_id', 'Taxonomy1');
    $this->drupalGet("de/node/$nid1/edit");
    $this->assertRaw('@DE@Erster');
    $nid2 = $this->getNidById('field_test_id', 'Taxonomy2');
    $this->drupalGet("de/node/$nid2/edit");
    $this->assertRaw('@DE@Zweiter');
  }

  /**
   * Imports a csv to update a German term.
   */
  private function updateGermanTerm() {

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/taxonomy-up-de.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // Show node pages.
    $nid = $this->getNidById('field_test_id', 'Taxonomy1');
    $this->drupalGet("node/$nid/edit");
    $this->assertRaw('@EN@First');
    $this->drupalGet("de/node/$nid/edit");
    $this->assertRaw('@DE@Erster aktualisiert');
  }

  /**
   * Configutes the importer for the test case.
   */
  private function configTaxonomy() {

    $edit = array();
    $edit['separator'] = ';';
    $edit['id'] = 'Id';
    $edit['contenttype'] = 'page';
    $edit['mapping'] = <<<EOF
Tag => field_test_tag
Id => field_test_id
EOF;
    $this->drupalPost('admin/config/system/simple-csv-importer/config', $edit, t('Save configuration'));
    $this->assertRaw(t('The configuration options have been saved.'));
    $this->assertNoMessages();
  }

}

/**
 * Test some scenarios with extensions.
 */
class SimpleCsvImporterExtendTestCase extends SimpleCsvImporterTestCase {

  private $initOk = TRUE;

  /**
   * Get info.
   */
  public static function getInfo() {

    return array(
      'name' => 'Import csv with ET for extended functionality',
      'description' => 'Import a csv with a text field using a separated module with extensions.',
      'group' => 'Simple CSV Importer',
    );
  }

  /**
   * Set up.
   */
  public function setUp() {

    parent::setUp();

    $this->initTest('init');
  }

  /**
   * Tear down.
   */
  public function tearDown() {

    $this->initTest('delete');
    parent::tearDown();
  }

  /**
   * Makes the extension module visible or removes it.
   */
  private function initTest($action) {

    $dir = dirname(__FILE__);
    $path = "$dir/simple_csv_importer_extend";

    if ($action == 'init') {
      $zip = new ZipArchive();
      $res = $zip->open($path . '.zip');
      $this->assertTrue(($res === TRUE), "Could not open zip file '$path.zip'.");
      if (!($res === TRUE)) {
        $this->initOk = FALSE;
        return;
      }

      $zip->extractTo($path);
      $zip->close();
    }
    else {
      // Does not work on windows, however.
      system("rm -rf $path");
    }
  }

  /**
   * Creates a content type with a text field.
   */
  protected function configureContentType() {

    parent::configureContentType();
    $this->addField('Test Info', 'text', TRUE/*translatable*/);
  }

  /**
   * Tests the case of an extension of the field class.
   */
  public function testExtension() {

    if (!$this->initOk) {
      return;
    }

    $this->configText(TRUE);
    $ok = module_enable(array('simple_csv_importer_extend'));
    $this->assertTrue($ok, t('The module "simple_csv_importer_extend" has been enabled'));

    $this->configText(FALSE);

    $edit = array();
    $edit['files[file]'] = dirname(__FILE__) . '/data/text-en.csv';
    $this->drupalPost('admin/config/system/simple-csv-importer/import', $edit, t('Save configuration'));
    $this->assertNoMessages();

    // Show node page.
    $nid = $this->getNidById('field_test_id', 'Text1');
    $this->drupalGet("node/$nid/edit");
    $this->assertRaw('@EN@THIS IS THE SIMPLE INFO.');
  }

  /**
   * Configures the importer for the test case.
   *
   * @param bool $is_class_error
   *   Whether we expect a class error.
   */
  private function configText($is_class_error) {

    $edit = array();
    $edit['separator'] = ';';
    $edit['id'] = 'Id';
    $edit['contenttype'] = 'page';
    $edit['mapping'] = <<<EOF
Info => field_test_info
Id => field_test_id
EOF;
    $edit['class_Field'] = '\\Drupal\\simple_csv_importer_extend\\ExtendField';
    $this->drupalPost('admin/config/system/simple-csv-importer/config', $edit, t('Save configuration'));
    if ($is_class_error) {
      $this->assertRaw('Did you enable your module and use a classname');
    }
    else {
      $this->assertRaw(t('The configuration options have been saved.'));
      $this->assertNoMessages();
    }
  }

}
