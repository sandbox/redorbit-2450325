CONTENTS OF THIS FILE
---------------------

* Introduction
* Features
* Requirements
* Installation
* Usage without multi language support
* Usage with multi language support
* Extended functionality
* Maintainers


INTRODUCTION
------------
The Simle CSV Importer module is an import and integration framework for
Drupal.

FEATURES
--------
* Import of csv files for creating nodes
* Update of nodes with csv file import
* Update of entity translation enabled nodes with language specific content
  (supports regular fields, file fields, image fields and taxonomy fields)


REQUIREMENTS
------------
This module requires the following module:
* XAutoload (https://www.drupal.org/project/xautoload)

If entity translation is desired the following modules are required:
* Entity Translation (with enabled locale module)
  (https://www.drupal.org/project/entity_translation)
* Title (https://www.drupal.org/project/title)


INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


USAGE WITHOUT MULTI LANGUAGE SUPPORT
------------------------------------
* General Workflow

  - Prepare a csv file with column name(s) in the first row.
  - Make sure to save the csv file as UTF-8 (!!) and do not use hyphens in
    file name.
  - Go to admin/config/system/simple-csv-importer/config und setup an importer
    (see below)
  - Go to admin/config/system/simple-csv-importer/import und upload your csv
    file

* Importer Setup

  - SEPARATOR and DELIMITER: Needs to be the same as used in csv file
  - ID:  Name of column in csv file which holds the unique id of every data set
  - CONTENT TYPE: Machine name of the content type
  - MAPPING: Contains the mapping values for the field names of the content
    type and the csv columns

* Mapping

  CSV-Column => Name of field (Drupal machine name)

  Example:

  Headline => title
  Content => body
  Categories => field_taxonomy
  Other Field => field_other


USAGE WITH MULTI LANGUAGE SUPPORT
---------------------------------
* Overview

  With the simple csv importer module you can also import translations for
  nodes with entity translation enabled. The following field types are
  supported:

  - Regular text fields
  - File fields
  - Image fields
  - Taxonomy fields

* Requirements

  - Entity Translation (with enabled locale module)
    (https://www.drupal.org/project/entity_translation)
  - Title (https://www.drupal.org/project/title)

* Recommended

  - Localization update (https://www.drupal.org/project/l10n_update)

* Workflow

  - Go to admin/regional/language und activate additional language(s)
  - Go to admin/config/regional/language/configure and configure language
    detection
  - Go to admin/config/regional/entity_translation and activate entity
    translation for nodes (default) and taxonomy (if you want to import
    language specific taxonomy values)
  - Go to admin/structure/types/manage/YOUR-CONTENT-TYPE "Publishing options"
    and activate under multilingual support "Enabled, with field translation"
  - Install and activate title module if you want the title to be translated
    as well (https://www.drupal.org/project/title)
  - Go to admin/structure/types/manage/YOUR-CONTENT-TYPE/fields and replace
    the title to get a title field. Enable entity translation for the fields
    you want them to be updated for every language from import (except taxonomy
    fields, see below).

    *Note:* If you are using taxonomy field(s) in a content type (term
            reference), leave these disabled for entity translation (you
            activate them in taxonomy settings, see below)
  - Go to admin/structure/taxonomy/YOUR-TAXONOMY/fields and replace the name
    (similar to the node title). Make sure the taxonomy name is enabled here
    for entity translation)

* Import a language specific version

  - in admin/config/system/simple-csv-importer/config make sure, that you
    modify the mapping for the replaced title field (if you are using the title
    field module). It should be now named "title_field"
  - Save your csv file for a specific language in UTF-8 with the following
    syntax: filename-de.csv (German) or filename-pt.csv (Portuguese) etc.
  - Upload your csv file at admin/config/system/simple-csv-importer/import



EXTENDED FUNCTIONALITY
----------------------
* The simple csv importer can be extended with own classes. Please refer to the
  EXTENSIONS.txt file in the module directory.


MAINTAINERS
-----------
Current maintainers:
* Siegfried Steinsiek - https://www.drupal.org/user/2238370
* Christian Heyer (redorbit) - https://www.drupal.org/user/1394872

This project has been sponsored by
* REDORBIT
  Specialized in planning and customizing Drupal sites and developing HbbTV
  web apps. Visit http://www.redorbit.de for more information.
